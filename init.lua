--[[

	GPLv3 Copyright (C) 2018 Hamlet <h4mlet@riseup.net>

	This program is free software; you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program; if not, write to the Free Software
	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
	MA 02110-1301, USA.

]]--


--
-- Constants
--

-- General

local LOW_WALK_CHANCE = 33
local MEDIUM_WALK_CHANCE = 50
local HIGH_WALK_CHANCE = 66

local DAMAGE_LIGHT_A = 2.308
local DAMAGE_LIGHT_B = 2.692
local DAMAGE_LIGHT_C = 3.846
local DAMAGE_LAVA = 99
local DAMAGE_WATER_A = 6.0
local DAMAGE_WATER_B = 7.0
local DAMAGE_WATER_C = 10.0
local ANIMATION = {
	speed_normal = 15,
	speed_run = 15,
	stand_start = 0,
	stand_end = 79,
	walk_start = 168,
	walk_end = 187,
	run_start = 168,
	run_end = 187,
	punch_start = 189,
	punch_end = 198,
}

local MESH = "mobs_sidhe_character.b3d"

local SOUNDS_PUNCH_ATTACK = {
	war_cry = "mobs_sidhe_die_yell",
	death = "mobs_sidhe_death",
	attack = "mobs_sidhe_punch",
}

local SOUNDS_SLASH_ATTACK = {
	war_cry = "mobs_sidhe_die_yell",
	death = "mobs_sidhe_death",
	attack = "mobs_sidhe_slash_attack",
}

-- Weapons

local BARE_HANDS_DAMAGE = 1
local BLUNT_DAMAGE = 2
local STEEL_PICK_DAMAGE = 4
local STEEL_SWORD_DAMAGE = 6
local BATTLE_AXE_DAMAGE = 7

-- Dwarves

local DWARF_WALK_VELOCITY = 2.5
local DWARF_RUN_VELOCITY = 3.5
local DWARF_VIEW_RANGE = 7
local DWARF_VISUAL_SIZE = {x = 1.1, y = 0.85}
local DWARF_COLLISION_BOX = {-0.3, -.85, -0.3, 0.3, 0.68, 0.3}
local HEIGHT_MAX_DWARF = -300
local HEIGHT_MIN_DWARF = -2700
local LIGHT_MAX_DWARF = 13
local LIGHT_MIN_DWARF = 1

local SPAWN_NODES_DWARF = {"default:stone",
	"castle_masonry:pavement_brick", "dfcaverns:dirt_with_cave_moss",
	"dfcaverns:cobble_with_floor_fungus",
}

-- Elves

local ELF_RUN_VELOCITY = 4.5
local ELF_VIEW_RANGE = 15
local ELF_VISUAL_SIZE = {x = 0.77, y = 0.85}
local HEIGHT_MAX_ELF = 300
local HEIGHT_MIN_ELF = 50
local LIGHT_MAX_ELF = 7
local LIGHT_MIN_ELF = 0

local SPAWN_NODES_ELF = {"default:stone", "default:dirt",
	"default:dirt_with_grass", "default:dirth_with_snow",
	"default:dirt_with_rainforest_litter",
}


--
-- Dwarf entities
--

-- Dwarf

mobs:register_mob("mobs_sidhe:dwarf", {
	nametag = "Dwarf Miner",
	type = "npc",
	hp_min = 20,
	hp_max = 35,
	armor = 100,
	walk_velocity = DWARF_WALK_VELOCITY,
	run_velocity = DWARF_RUN_VELOCITY,
	walk_chance = MEDIUM_WALK_CHANCE,
	jump = false,
	fly = false,
	view_range = DWARF_VIEW_RANGE,
	reach = 4,
	damage = STEEL_PICK_DAMAGE,
	fear_height = 3,
	fall_speed = -10,
	fall_damage = true,
	water_damage = DAMAGE_WATER_B,
	lava_damage = DAMAGE_LAVA,
	light_damage = DAMAGE_LIGHT_B,
	suffocation = true,
	floats = 0,
	attacks_monsters = true,
	group_attack = true,
	attack_type = "dogfight",
	runaway_from = {
		"mobs:balrog",
		"mobs_monster:mese_arrow",
		"mobs_monster:fireball",
	},
	makes_footstep_sound = true,
	sounds = SOUNDS_PUNCH_ATTACK,
	drops = {
		{
			name = "default:pick_steel",
			chance = 10,
			min = 1,
			max = 1,
		},

		{
			name = "default:torch",
			chance = 10,
			min = 1,
			max = 12,
		},
	},
	visual = "mesh",
	visual_size = DWARF_VISUAL_SIZE,
	collisionbox = DWARF_COLLISION_BOX,
	textures = {
		{"mobs_sidhe_dwarf.png", "mobs_sidhe_trans.png",
		"default_tool_steelpick.png", "mobs_sidhe_trans.png",
		"mobs_sidhe_trans.png"},

		{"mobs_sidhe_dwarf_1.png", "mobs_sidhe_trans.png",
		"default_tool_steelpick.png", "mobs_sidhe_trans.png",
		"mobs_sidhe_trans.png"},

		{"mobs_sidhe_dwarf_2.png", "mobs_sidhe_trans.png",
		"default_tool_steelpick.png", "mobs_sidhe_trans.png",
		"mobs_sidhe_trans.png"},

		{"mobs_sidhe_dwarf_3.png", "mobs_sidhe_trans.png",
		"default_tool_steelpick.png", "mobs_sidhe_trans.png",
		"mobs_sidhe_trans.png"},
	},
	mesh = MESH,
	animation = ANIMATION,
	replace_what = {"default:stone_with_coal", "default:stone_with_iron",
		"default:stone_with_copper", "default:stone_with_tin",
		"default:stone_with_mese", "default:stone_with_gold",
		"default:stone_with_diamond",
		"real_minerals:anthracite_in_default_stone",
		"real_minerals:bauxite_in_default_stone",
		"real_minerals:bismuthinite_in_default_stone",
		"real_minerals:bituminous_coal_in_default_stone",
		"real_minerals:cassiterite_in_default_stone",
		"real_minerals:cinnabar_in_default_stone",
		"real_minerals:cryolite_in_default_stone",
		"real_minerals:galena_in_default_stone",
		"real_minerals:garnierite_in_default_stone",
		"real_minerals:graphite_in_default_stone",
		"real_minerals:gypsum_in_default_stone",
		"real_minerals:hematite_in_default_stone",
		"real_minerals:jet_in_default_stone",
		"real_minerals:kaolinite_in_default_stone",
		"real_minerals:kimberlite_in_default_stone",
		"real_minerals:lazurite_in_default_stone",
		"real_minerals:lignite_in_default_stone",
		"real_minerals:limonite_in_default_stone",
		"real_minerals:magnetite_in_default_stone",
		"real_minerals:malachite_in_default_stone",
		"real_minerals:native_copper_in_default_stone",
		"real_minerals:native_copper_in_desert_stone",
		"real_minerals:native_gold_in_default_stone",
		"real_minerals:native_gold_in_desert_stone",
		"real_minerals:native_platinum_in_default_stone",
		"real_minerals:native_silver_in_default_stone",
		"real_minerals:olivine_in_default_stone",
		"real_minerals:petrified_wood_in_default_stone",
		"real_minerals:pitchblende_in_default_stone",
		"real_minerals:saltpeter_in_default_stone",
		"real_minerals:satin_spar_in_default_stone",
		"real_minerals:selenite_in_default_stone",
		"real_minerals:serpentine_in_default_stone",
		"real_minerals:sphalerite_in_default_stone",
		"real_minerals:sylvite_in_default_stone",
		"real_minerals:tenorite_in_default_stone",
		"real_minerals:tetrahedrite_in_default_stone",
	},
	replace_with = "default:cobble",
	replace_rate = 6,
	replace_offset = -1,
	on_replace = function(self, pos, oldnode, newnode)
		local position = {x = pos.x, y = (pos.y +1), z = pos.z}
		minetest.set_node(position, {name="default:torch", param2 = 1})
	end,
})

-- Dwarf Soldier

mobs:register_mob("mobs_sidhe:dwarf_soldier", {
	nametag = "Dwarf Soldier",
	type = "npc",
	hp_min = 35,
	hp_max = 50,
	armor = 85,
	walk_velocity = DWARF_WALK_VELOCITY,
	run_velocity = DWARF_RUN_VELOCITY,
	walk_chance = LOW_WALK_CHANCE,
	jump = false,
	fly = false,
	view_range = DWARF_VIEW_RANGE,
	reach = 4,
	damage = STEEL_SWORD_DAMAGE,
	fear_height = 3,
	fall_speed = -10,
	fall_damage = true,
	water_damage = DAMAGE_WATER_C,
	lava_damage = DAMAGE_LAVA,
	light_damage = DAMAGE_LIGHT_C,
	suffocation = true,
	floats = 0,
	attacks_monsters = true,
	group_attack = true,
	attack_type = "dogfight",
	runaway_from = {
		"mobs:balrog",
	},
	makes_footstep_sound = true,
	sounds = SOUNDS_SLASH_ATTACK,
	drops = {
		{
			 name = "default:sword_steel",
			 chance = 10,
			 min = 1,
			 max = 1,
		},
		{
			 name = "shields:shield_steel",
			 chance = 10,
			 min = 1,
			 max = 1,
		},
	},
	visual = "mesh",
	visual_size = DWARF_VISUAL_SIZE,
	collisionbox = DWARF_COLLISION_BOX,
	textures = {

	 {"mobs_sidhe_dwarf.png",
	 "mobs_sidhe_helmet_steel.png^mobs_sidhe_chestplate_steel.png^mobs_sidhe_leggings_steel.png^mobs_sidhe_boots_steel.png^mobs_sidhe_shield_steel.png",
	 "default_tool_steelsword.png", "mobs_sidhe_trans.png"},

	 {"mobs_sidhe_dwarf_1.png",
	 "mobs_sidhe_helmet_steel.png^mobs_sidhe_chestplate_steel.png^mobs_sidhe_leggings_steel.png^mobs_sidhe_boots_steel.png^mobs_sidhe_shield_steel.png",
	 "default_tool_steelsword.png", "mobs_sidhe_trans.png"},

	 {"mobs_sidhe_dwarf_2.png",
	 "mobs_sidhe_helmet_steel.png^mobs_sidhe_chestplate_steel.png^mobs_sidhe_leggings_steel.png^mobs_sidhe_boots_steel.png^mobs_sidhe_shield_steel.png",
	 "default_tool_steelsword.png", "mobs_sidhe_trans.png"},

	 {"mobs_sidhe_dwarf_3.png",
	 "mobs_sidhe_helmet_steel.png^mobs_sidhe_chestplate_steel.png^mobs_sidhe_leggings_steel.png^mobs_sidhe_boots_steel.png^mobs_sidhe_shield_steel.png",
	 "default_tool_steelsword.png", "mobs_sidhe_trans.png"},
	},
	mesh = MESH,
	animation = ANIMATION,
})

-- Dwarf Paladine

mobs:register_mob("mobs_sidhe:dwarf_paladine", {
	nametag = "Dwarf Paladine",
	type = "npc",
	hp_min = 35,
	hp_max = 50,
	armor = 70,
	walk_velocity = DWARF_WALK_VELOCITY,
	run_velocity = DWARF_RUN_VELOCITY,
	walk_chance = HIGH_WALK_CHANCE,
	jump = false,
	fly = false,
	view_range = DWARF_VIEW_RANGE,
	reach = 4,
	damage = BATTLE_AXE_DAMAGE,
	fear_height = 3,
	fall_speed = -10,
	fall_damage = true,
	water_damage = DAMAGE_WATER_C,
	lava_damage = DAMAGE_LAVA,
	light_damage = DAMAGE_LIGHT_C,
	suffocation = true,
	floats = 0,
	attacks_monsters = true,
	group_attack = true,
	attack_type = "dogfight",
	makes_footstep_sound = true,
	sounds = SOUNDS_SLASH_ATTACK,
	drops = {
		{
			 name = "castle_weapons:battleaxe",
			 chance = 10,
			 min = 1,
			 max = 1,
		},
		{
			 name = "shields:shield_gold",
			 chance = 10,
			 min = 1,
			 max = 1,
		},
	},
	visual = "mesh",
	visual_size = DWARF_VISUAL_SIZE,
	collisionbox = DWARF_COLLISION_BOX,
	textures = {

	 {"mobs_sidhe_dwarf.png",
	 "mobs_sidhe_helmet_gold.png^mobs_sidhe_chestplate_gold.png^mobs_sidhe_leggings_gold.png^mobs_sidhe_boots_gold.png^mobs_sidhe_shield_gold.png",
	 "mobs_sidhe_battleaxe.png", "mobs_sidhe_trans.png"},

	 {"mobs_sidhe_dwarf_1.png",
	 "mobs_sidhe_helmet_gold.png^mobs_sidhe_chestplate_gold.png^mobs_sidhe_leggings_gold.png^mobs_sidhe_boots_gold.png^mobs_sidhe_shield_gold.png",
	 "mobs_sidhe_battleaxe.png", "mobs_sidhe_trans.png"},

	 {"mobs_sidhe_dwarf_2.png",
	 "mobs_sidhe_helmet_gold.png^mobs_sidhe_chestplate_gold.png^mobs_sidhe_leggings_gold.png^mobs_sidhe_boots_gold.png^mobs_sidhe_shield_gold.png",
	 "mobs_sidhe_battleaxe.png", "mobs_sidhe_trans.png"},

	 {"mobs_sidhe_dwarf_3.png",
	 "mobs_sidhe_helmet_gold.png^mobs_sidhe_chestplate_gold.png^mobs_sidhe_leggings_gold.png^mobs_sidhe_boots_gold.png^mobs_sidhe_shield_gold.png",
	 "mobs_sidhe_battleaxe.png", "mobs_sidhe_trans.png"},
	},
	mesh = MESH,
	animation = ANIMATION,
})

-- Dwarf Marksman

mobs:register_mob("mobs_sidhe:dwarf_marksman", {
	nametag = "Dwarf Marksman",
	type = "npc",
	hp_min = 20,
	hp_max = 35,
	armor = 100,
	walk_velocity = DWARF_WALK_VELOCITY,
	run_velocity = DWARF_RUN_VELOCITY,
	walk_chance = MEDIUM_WALK_CHANCE,
	jump = false,
	fly = false,
	view_range = DWARF_VIEW_RANGE,
	reach = 4,
	damage = BLUNT_DAMAGE,
	fear_height = 3,
	fall_speed = -10,
	fall_damage = true,
	water_damage = DAMAGE_WATER_B,
	lava_damage = DAMAGE_LAVA,
	light_damage = DAMAGE_LIGHT_B,
	suffocation = true,
	floats = 0,
	attacks_monsters = true,
	group_attack = true,
	attack_type = "dogshoot",
	arrow = "mobs_sidhe:crossbow_bolt",
	shoot_offset = 1.9,
	shoot_interval = 3,
	runaway_from = {
		"mobs:balrog",
	},
	makes_footstep_sound = true,
	sounds = {
		random = "mobs_sidhe_crossbow_reload",
		war_cry = "mobs_sidhe_die_yell",
	 	death = "mobs_sidhe_death",
	 	attack = "mobs_sidhe_punch",
		shoot_attack = "mobs_sidhe_crossbow_click",
	},
	drops = {
		{
			 name = "castle_weapons:crossbow",
			 chance = 10,
			 min = 1,
			 max = 1,
		},
		{
			 name = "castle_weapons:crossbow_bolt",
			 chance = 10,
			 min = 1,
			 max = 12,
		},
	},
	visual = "mesh",
	visual_size = DWARF_VISUAL_SIZE,
	collisionbox = DWARF_COLLISION_BOX,
	textures = {
		{"mobs_sidhe_dwarf.png", "mobs_sidhe_trans.png",
		"mobs_sidhe_crossbow_loaded.png^[transformR45",
		"mobs_sidhe_trans.png",},

		{"mobs_sidhe_dwarf_1.png", "mobs_sidhe_trans.png",
		"mobs_sidhe_crossbow_loaded.png^[transformR45",
		"mobs_sidhe_trans.png",},

		{"mobs_sidhe_dwarf_2.png", "mobs_sidhe_trans.png",
		"mobs_sidhe_crossbow_loaded.png^[transformR45",
		"mobs_sidhe_trans.png",},

		{"mobs_sidhe_dwarf_3.png", "mobs_sidhe_trans.png",
		"mobs_sidhe_crossbow_loaded.png^[transformR45",
		"mobs_sidhe_trans.png",},
	},
	mesh = MESH,
	animation = ANIMATION,
})


--
-- Elf entities
--

-- Elf

mobs:register_mob("mobs_sidhe:elf", {
	nametag = "Elf",
	type = "monster",
	hp_min = 15,
	hp_max = 30,
	armor = 100,
	walk_velocity = DWARF_WALK_VELOCITY,
	run_velocity = ELF_RUN_VELOCITY,
	walk_chance = MEDIUM_WALK_CHANCE,
	jump = false,
	fly = false,
	view_range = ELF_VIEW_RANGE,
	reach = 4,
	damage = BARE_HANDS_DAMAGE,
	fear_height = 6,
	fall_speed = -10,
	fall_damage = true,
	water_damage = DAMAGE_WATER_A,
	lava_damage = DAMAGE_LAVA,
	light_damage = DAMAGE_LIGHT_A,
	suffocation = true,
	floats = 0,
	docile_by_day = false,
	attack_animals = true,
	group_attack = true,
	attack_type = "dogfight",
	pathfinding = 1,
	makes_footstep_sound = true,
	sounds = SOUNDS_PUNCH_ATTACK,
	visual = "mesh",
	visual_size = ELF_VISUAL_SIZE,
	collisionbox = DWARF_COLLISION_BOX,
	textures = {
		{"mobs_sidhe_elf.png", "mobs_sidhe_boots_elf.png",
		"mobs_sidhe_trans.png", "mobs_sidhe_trans.png",},

		{"mobs_sidhe_elf_1.png", "mobs_sidhe_boots_elf.png",
		"mobs_sidhe_trans.png", "mobs_sidhe_trans.png",},

		{"mobs_sidhe_elf_2.png", "mobs_sidhe_boots_elf.png",
		"mobs_sidhe_trans.png", "mobs_sidhe_trans.png",},
	},
	mesh = MESH,
	animation = ANIMATION,
})

mobs:register_mob("mobs_sidhe:elf_swordman", {
	nametag = "Elf Swordman",
	type = "monster",
	hp_min = 20,
	hp_max = 30,
	armor = 100,
	walk_velocity = DWARF_WALK_VELOCITY,
	run_velocity = ELF_RUN_VELOCITY,
	walk_chance = HIGH_WALK_CHANCE,
	jump = false,
	fly = false,
	view_range = ELF_VIEW_RANGE,
	reach = 4,
	damage = STEEL_SWORD_DAMAGE,
	fear_height = 6,
	fall_speed = -10,
	fall_damage = true,
	water_damage = DAMAGE_WATER_A,
	lava_damage = DAMAGE_LAVA,
	light_damage = DAMAGE_LIGHT_A,
	suffocation = true,
	floats = 0,
	docile_by_day = false,
	attack_animals = true,
	group_attack = true,
	attack_type = "dogfight",
	pathfinding = 1,
	makes_footstep_sound = true,
	sounds = SOUNDS_SLASH_ATTACK,
	drops = {
		{
			 name = "default:sword_steel",
			 chance = 10,
			 min = 1,
			 max = 1,
		},
	},
	visual = "mesh",
	visual_size = ELF_VISUAL_SIZE,
	collisionbox = DWARF_COLLISION_BOX,
	textures = {
		{"mobs_sidhe_elf.png",
		"mobs_sidhe_chestplate_elf.png^mobs_sidhe_boots_elf.png",
		"default_tool_steelsword.png", "mobs_sidhe_trans.png",},

		{"mobs_sidhe_elf_1.png",
		"mobs_sidhe_chestplate_elf.png^mobs_sidhe_boots_elf.png",
		"default_tool_steelsword.png", "mobs_sidhe_trans.png",},

		{"mobs_sidhe_elf_2.png",
		"mobs_sidhe_chestplate_elf.png^mobs_sidhe_boots_elf.png",
		"default_tool_steelsword.png", "mobs_sidhe_trans.png",},
	},
	mesh = MESH,
	animation = ANIMATION,
})

mobs:register_mob("mobs_sidhe:elf_archer", {
	nametag = "Elf Archer",
	type = "monster",
	hp_min = 20,
	hp_max = 30,
	armor = 100,
	walk_velocity = DWARF_WALK_VELOCITY,
	run_velocity = ELF_RUN_VELOCITY,
	walk_chance = HIGH_WALK_CHANCE,
	jump = false,
	fly = false,
	view_range = ELF_VIEW_RANGE,
	reach = 4,
	damage = BLUNT_DAMAGE,
	fear_height = 6,
	fall_speed = -10,
	fall_damage = true,
	water_damage = DAMAGE_WATER_A,
	lava_damage = DAMAGE_LAVA,
	light_damage = DAMAGE_LIGHT_A,
	suffocation = true,
	floats = 0,
	docile_by_day = false,
	attack_animals = true,
	group_attack = true,
	attack_type = "dogshoot",
	arrow = "mobs_sidhe:bow_arrow",
	shoot_offset = 1.9,
	shoot_interval = 1.5,
	pathfinding = 1,
	makes_footstep_sound = true,
	sounds = {
		war_cry = "mobs_sidhe_die_yell",
	 	death = "mobs_sidhe_death",
	 	attack = "mobs_sidhe_punch",
		shoot_attack = "mobs_sidhe_bow",
	},
	drops = {
		{
			 name = "archer:bow_bronze",
			 chance = 10,
			 min = 1,
			 max = 1,
		},
		{
			 name = "archer:steel_arrow",
			 chance = 10,
			 min = 1,
			 max = 12,
		},
	},
	visual = "mesh",
	visual_size = ELF_VISUAL_SIZE,
	collisionbox = DWARF_COLLISION_BOX,
	textures = {
		{"mobs_sidhe_elf.png",
		"mobs_sidhe_chestplate_elf.png^mobs_sidhe_boots_elf.png",
		"mobs_sidhe_bow_bronze.png", "mobs_sidhe_trans.png",},

		{"mobs_sidhe_elf_1.png",
		"mobs_sidhe_chestplate_elf.png^mobs_sidhe_boots_elf.png",
		"mobs_sidhe_bow_bronze.png", "mobs_sidhe_trans.png",},

		{"mobs_sidhe_elf_2.png",
		"mobs_sidhe_chestplate_elf.png^mobs_sidhe_boots_elf.png",
		"mobs_sidhe_bow_bronze.png", "mobs_sidhe_trans.png",},
	},
	mesh = MESH,
	animation = ANIMATION,
})

--
-- Projectiles
--

-- Crossbow Bolt

mobs:register_arrow("mobs_sidhe:crossbow_bolt", {

	visual = "sprite",
	visual_size = {x = 1, y = 1},
	textures = {"mobs_sidhe_crossbow_bolt.png"},
	--rotate = 45,
	velocity = 10,
	drop = false,

	hit_player = function(self, player)
		local pos = self.object:get_pos()
		minetest.sound_play("mobs_sidhe_shoot", {pos = pos})
		player:punch(self.object, 1.0, {
			full_punch_interval = 1.0,
			damage_groups = {fleshy = 6},
		}, nil)
	end,

	hit_mob = function(self, player)
		local pos = self.object:get_pos()
		minetest.sound_play("mobs_sidhe_shoot", {pos = pos})
		player:punch(self.object, 1.0, {
			full_punch_interval = 1.0,
			damage_groups = {fleshy = 6},
		}, nil)
	end,

	hit_node = function(self, pos, node)
		self.object:remove()
	end
})

-- Bow arrow

mobs:register_arrow("mobs_sidhe:bow_arrow", {

	visual = "sprite",
	visual_size = {x = 0.7, y = 0.7},
	textures = {"mobs_sidhe_arrow_steel.png"},
	--rotate = 45,
	velocity = 10,
	drop = false,

	hit_player = function(self, player)
		local pos = self.object:get_pos()
		minetest.sound_play("mobs_sidhe_arrow", {pos = pos})
		player:punch(self.object, 1.0, {
			full_punch_interval = 1.0,
			damage_groups = {fleshy = 6},
		}, nil)
	end,

	hit_mob = function(self, player)
		local pos = self.object:get_pos()
		minetest.sound_play("mobs_sidhe_arrow", {pos = pos})
		player:punch(self.object, 1.0, {
			full_punch_interval = 1.0,
			damage_groups = {fleshy = 6},
		}, nil)
	end,

	hit_node = function(self, pos, node)
		self.object:remove()
	end
})

--
-- Dwarves spawns
--

mobs:spawn({name = "mobs_sidhe:dwarf",
	nodes = SPAWN_NODES_DWARF,
	neighbors = {"air",},
	max_light = LIGHT_MAX_DWARF,
	min_light = LIGHT_MIN_DWARF,
	interval = 30,
	chance = 8000,
	active_object_count = 2,
	min_height = HEIGHT_MIN_DWARF,
	max_height = HEIGHT_MAX_DWARF,
	-- day_toggle = true,
	-- on_spawn = ,
})

mobs:spawn({name = "mobs_sidhe:dwarf_soldier",
	nodes = SPAWN_NODES_DWARF,
	neighbors = {"air",},
	max_light = LIGHT_MAX_DWARF,
	min_light = LIGHT_MIN_DWARF,
	interval = 30,
	chance = 40000,
	active_object_count = 2,
	min_height = HEIGHT_MIN_DWARF,
	max_height = HEIGHT_MAX_DWARF,
	-- day_toggle = true,
	-- on_spawn = ,
})

mobs:spawn({name = "mobs_sidhe:dwarf_paladine",
	nodes = SPAWN_NODES_DWARF,
	neighbors = {"air",},
	max_light = LIGHT_MAX_DWARF,
	min_light = LIGHT_MIN_DWARF,
	interval = 30,
	chance = 120000,
	active_object_count = 2,
	min_height = HEIGHT_MIN_DWARF,
	max_height = HEIGHT_MAX_DWARF,
	-- day_toggle = true,
	-- on_spawn = ,
})

mobs:spawn({name = "mobs_sidhe:dwarf_marksman",
	nodes = SPAWN_NODES_DWARF,
	neighbors = {"air",},
	max_light = LIGHT_MAX_DWARF,
	min_light = LIGHT_MIN_DWARF,
	interval = 30,
	chance = 40000,
	active_object_count = 2,
	min_height = HEIGHT_MIN_DWARF,
	max_height = HEIGHT_MAX_DWARF,
	-- day_toggle = true,
	on_spawn = function(self)
		local pos = self.object:get_pos()
		minetest.sound_play("mobs_sidhe_crossbow_reload", {pos = pos})
	end,
})


--
-- Elves' spawns
--

mobs:spawn({name = "mobs_sidhe:elf",
	nodes = SPAWN_NODES_ELF,
	neighbors = {"air",},
	max_light = LIGHT_MAX_ELF,
	min_light = LIGHT_MIN_ELF,
	interval = 30,
	chance = 8000,
	active_object_count = 2,
	min_height = HEIGHT_MIN_ELF,
	max_height = HEIGHT_MAX_ELF,
	-- day_toggle = true,
	-- on_spawn = ,
})

mobs:spawn({name = "mobs_sidhe:elf_swordman",
	nodes = SPAWN_NODES_ELF,
	neighbors = {"air",},
	max_light = LIGHT_MAX_ELF,
	min_light = LIGHT_MIN_ELF,
	interval = 30,
	chance = 50000,
	active_object_count = 2,
	min_height = HEIGHT_MIN_ELF,
	max_height = HEIGHT_MAX_ELF,
	-- day_toggle = true,
	-- on_spawn = ,
})

mobs:spawn({name = "mobs_sidhe:elf_archer",
	nodes = SPAWN_NODES_ELF,
	neighbors = {"air",},
	max_light = LIGHT_MAX_ELF,
	min_light = LIGHT_MIN_ELF,
	interval = 30,
	chance = 50000,
	active_object_count = 2,
	min_height = HEIGHT_MIN_ELF,
	max_height = HEIGHT_MAX_ELF,
	-- day_toggle = true,
	-- on_spawn = ,
})

mobs:alias_mob("mobs:dwarf", "mobs_sidhe:dwarf")
mobs:alias_mob("mobs:dwarf_soldier", "mobs_sidhe:dwarf_soldier")
mobs:alias_mob("mobs:dwarf_paladine", "mobs_sidhe:dwarf_paladine")
mobs:alias_mob("mobs:dwarf_marksman", "mobs_sidhe:dwarf_marksman")
mobs:alias_mob("mobs:elf", "mobs_sidhe:elf")
mobs:alias_mob("mobs:elf_swordman", "mobs_sidhe:elf_swordman")
mobs:alias_mob("mobs:elf_archer", "mobs_sidhe:elf_archer")
